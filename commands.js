#!/usr/bin/env node
const program = require('commander');
const package = require('./package.json');
const {convertIntToString} = require('./index.js');

program
    .version(package.version)
    .description(package.description);

program
    .argument('<number>')
    .action((input) => {
        const words = convertIntToString(input);
        console.log(words)
    });

program.parse(process.argv);