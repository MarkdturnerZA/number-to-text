# README #

### What is this repository for? ###
* CLI to convert numbers into words.
* Version 1.0.0

### How do I get set up? ###

* CD into repo directory.
* $ npm link
* $ number-to-words <number input>
