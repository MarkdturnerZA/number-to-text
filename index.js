

/**
 * 1 - single
 * 20 - tenths
 * 100 - hundred
 * 1000 - thousand
 * 10000 - ten thousand
 * 100000 - hundred thousand
 */

const TEN = 10;
const ONE_HUNDRED = 100;
const ONE_THOUSAND = 1000;
const ONE_MILLION = 1000000;

const LESS_THAN_TWENTY = [
    'zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten',
    'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
];

const TENTHS_LESS_THAN_HUNDRED = [
    'zero', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'
];

let wordAccumulator = '';
let remainder = 0;

const generateWords = ( number ) => {
    
    if ( number === 0 ) {
        return wordAccumulator;
    }

    if (number < 20 ) {

        remainder = number % 1;
        wordAccumulator +=  LESS_THAN_TWENTY[number];
        generateWords(remainder);

    } else if ( number < ONE_HUNDRED ) {
        remainder = number % TEN;
        suffix = Math.floor(number / TEN);
        if ( remainder === 0 ) { 
            wordAccumulator += TENTHS_LESS_THAN_HUNDRED[suffix]
        } else {
            wordAccumulator += TENTHS_LESS_THAN_HUNDRED[suffix] + '-';
        }
        
        generateWords(remainder);

    } else if ( number < ONE_THOUSAND ) {

        remainder = number % ONE_HUNDRED;
        suffix = Math.floor(number / ONE_HUNDRED);
        wordAccumulator += LESS_THAN_TWENTY[suffix] + ' hundrend';

        if ( remainder !== 0 ) {
            wordAccumulator += ',';
        }
        
        generateWords(remainder);

    }   else if ( number < ONE_MILLION ) {

        remainder = number % ONE_THOUSAND;
        suffix = Math.floor(number / ONE_THOUSAND);
        const tenthsSuffix = Math.floor(suffix / 10);
        const HundredsSuffix = suffix % 10;

        if(tenthsSuffix < 2) {
            wordAccumulator += LESS_THAN_TWENTY[suffix] + ' thousand ';
        } else if( HundredsSuffix > 0) {
            wordAccumulator += TENTHS_LESS_THAN_HUNDRED[tenthsSuffix] + '-' + LESS_THAN_TWENTY[HundredsSuffix] + ' thousand ';
        } else if ( suffix === ONE_HUNDRED ) {
            wordAccumulator += LESS_THAN_TWENTY[suffix / ONE_HUNDRED] + ' hundred thousand';
        } else {
            wordAccumulator += TENTHS_LESS_THAN_HUNDRED[tenthsSuffix] + ' thousand ';
        }

        if ( remainder < ONE_HUNDRED && remainder !== 0) {
            wordAccumulator += ',';
        }
   
        generateWords(remainder);
    }

 
    return wordAccumulator
}

const replaceOccurrence = (string, regex, occurence, replace) => {
    let i = 0;
    return string.replace(regex, (match) => {
        i += 1;
        if(i === occurence ) return replace;
        return match;
    });
}

const convertIntToString = (number) => {
    const words = generateWords(number);
    const formatedWords = replaceOccurrence(words, /,/g, 1, ' and ');

    return formatedWords
}

module.exports = { convertIntToString };